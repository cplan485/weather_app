import { StatusBar } from "expo-status-bar";
import React from "react";
import { View, Text, Image, TouchableOpacity, ScrollView, Modal, Linking, Switch, SegmentedControlIOS } from "react-native";
import Constants from "expo-constants";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import LoadingIcon from "./components/LoadingIcon";
import Styling from "./components/Styling";
import StylingNight from "./components/StylingNight";
import Forecast from "./components/Forecast";
import HourlyForecast from "./components/HourlyForecast";
import MainWeather from "./components/MainWeather";
//import { AnimatedBackgroundColorView } from 'react-native-animated-background-color-view';
import Icon from "react-native-vector-icons/AntDesign";
import LoadingScene from "./components/LoadingScene";
import World from "./assets/world.png";

const GOOGLE_PLACES_API_KEY = "AIzaSyBMM_9moMb0YT44mtEZnzqjJReeFFHJwqg"; // never save your real api key in a snack!

export default class App extends React.Component {
  state = {
    lat: "",
    lon: "",
    temperature: "",
    description: "",
    backupDescription: '',
    cityName: "",
    high: "",
    low: "",
    windspeed: "",
    iconAnimating: true,
    backgroundColor: "",
    showSearch: false,
    showAbout: false,
    forecast: [],
    newSearch: "",
    modalVisible: false,
    hourlyArr: [],
    sunrise: "",
    sunset: "",
    currentDt: "",
    country: "",
    holiday: "",
    toggle: false,
    showSplash: true,
  };

  componentDidMount() {
    let self= this;
    this.getCurrentWeather();
    this.getForecastWeather();
    
   setTimeout(
  function() {
      this.setState({showSplash: false});
  }
  .bind(this),
  3000
);
  }

 convertToFarenheit(num) {
    return Math.round((num * 9) / 5 - 459.67);
  }

  convertToCelsius(F) {
    return Math.round((5/9) * (F - 32));
  }

  convertToFfromCelsius(num) {
    return Math.round((num * 1.8) + 32);
  }

changeSplash() {
  this.setState({showSplash: !this.state.showSplash});

}

    convertAllValsToCelsius() {
    if (this.state.toggle==false) {
    
    let hourlyDup = [...this.state.hourlyArr];
    let forecastDup = [...this.state.forecast];
    forecastDup.forEach((item) => {
      item.daytemp = this.convertToCelsius(item.daytemp)
      item.maxTemp = this.convertToCelsius(item.maxTemp)
      item.minTemp = this.convertToCelsius(item.minTemp)
    })

    hourlyDup.forEach((item => {
      if(!item.sunrise || item.sunset) {
        item.temp = this.convertToCelsius(item.temp)
        
      }

    }))
    this.setState({
      temperature: this.convertToCelsius(this.state.temperature),
      high: this.convertToCelsius(this.state.high),
      low: this.convertToCelsius(this.state.low),
      forecast: forecastDup,
      hourlyArr: hourlyDup,
    })
    }
    else {
      let hourlyDup = [...this.state.hourlyArr];
      let forecastDup = [...this.state.forecast];
       forecastDup.forEach((item) => {
      item.daytemp = this.convertToFfromCelsius(item.daytemp)
      item.maxTemp = this.convertToFfromCelsius(item.maxTemp)
      item.minTemp = this.convertToFfromCelsius(item.minTemp)
    })
    hourlyDup.forEach((item => {
      if(!item.sunrise || item.sunset) {
        item.temp = this.convertToFfromCelsius(item.temp)
        
      }

    }))
      this.setState({
      temperature: this.convertToFfromCelsius(this.state.temperature),
      high: this.convertToFfromCelsius(this.state.high),
      low: this.convertToFfromCelsius(this.state.low),
      forecast: forecastDup
    })
    }
    
  }

  toggleSwitch() {
    this.setState({toggle: !this.state.toggle});
    this.convertAllValsToCelsius();
  }



  getCurrentWeather = async () => {
    let self = this;
    const { convertToFarenheit} = this;
    function success(position) {
      let weatherObj = {};
      const lat = position.coords.latitude;
      const lon = position.coords.longitude;
      self.setState({ lat: lat });
      self.setState({ lon: lon });
      let key = "87d1c5e946728f6a93e632277bb15dc4";
      const api = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${key}`;
      let myWeather = fetch(api)
        .then((response) => response.json())
        .then((data) => {
          // CALENDARIFIC API console.log(`CURRENT WEATHER DATA`, data.sys.country)
          const { name, weather, sys, main, wind } = data;
          //console.log("checking data,", data)
          weatherObj.name = name + ", ==> name of the city";
          self.setState({ cityName: name });
          weatherObj.description =
            data.weather[0].description + ", ==> weather description";
          if (weather[0].description.includes("cloud")
          || weather[0].description.includes("haze")
          || weather[0].description.includes("mist")
          || weather[0].description.includes("fog")
          ) {
            self.setState({ backgroundColor: "#ADAA9E" });
          } else if (weatherObj.description.includes("clear")) {
            self.setState({ backgroundColor: "#FFC933" });
          } else if (weatherObj.description.includes("rain") || weatherObj.description.includes("smoke")) {
            self.setState({ backgroundColor: "#E0E3E4" });
          } else if (weatherObj.description.includes("snow")) {
            self.setState({ backgroundColor: "#FFFFFF" });
          } else {
            self.setState({ backgroundColor: "E0E3E4" });
          }

          self.setState({ country: sys.country });
          self.setState({ description: weather[0].description });
          weatherObj.temperature =
            convertToFarenheit(main.temp) + ", ==> temperature";
          weatherObj.temperature_min =
            convertToFarenheit(main.temp_min) + ", ==> temperature min";
          self.setState({ low: self.convertToFarenheit(main.temp_min) });
          weatherObj.temperature_max =
            convertToFarenheit(main.temp_max) + ", ==> temperature max";
          self.setState({ high: convertToFarenheit(main.temp_max) });
          self.state.temperature = convertToFarenheit(main.temp);
          self.setState({ temperature: convertToFarenheit(main.temp) });
          weatherObj.wind = wind.speed + ", ==> wind speed";
          self.setState({ windspeed: wind.speed });
          weatherObj.humidity = main.humidity + ", ==> humidity";
          self.getHoliday();
          self.getHourlyWeather();
          self.setState({
            iconAnimating: false,
          });

        })
        .catch((err) => console.log("error retrieiving!"));
    }

    function error() {
      return "Unable to retrieve your location";
    }

    navigator.geolocation.getCurrentPosition(success, error);
    
  };

  getHoliday = async () => {
    let self = this;

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();
    
    let date = yyyy + "-" + mm + "-" + dd;
    var myHeaders = new Headers();
    myHeaders.append(
      "Cookie",
      "__cfduid=dc2e7d375a1b07f4fd04c4ad1ec794fa61611252610; PHPSESSID=ia2v1uj6te5qn9uk88fcm18uui"
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    let country = this.state.country;
    let key = "8248da7a4eb1511e4ccd41293a7f2ab7592702be";
    const api = `https://calendarific.com/api/v2/holidays?&api_key=${key}&country=${country}&year=2021`;
    fetch(api, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        // console.log(`CALENDARIFIC`, data.response.holidays)
        data.response.holidays.forEach((item) => {
          if (item.date.iso === date) {
            self.setState({
              holiday: `Today is ${item.name} in ${item.country.name}!`,
            });
          }
        });
      })
      .catch((err) => console.log("Calendar is giving an error!"));
  };

  getWeekDay(date) {
    const dayOfWeek = new Date(date).getDay();
    return isNaN(dayOfWeek)
      ? null
      : ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"][dayOfWeek];
  }

  getForecastWeather = async () => {
    let self = this;
    const { convertToFarenheit, state } = this;
    function success(position) {
      let forecastArr = [];
      const lat = state.lat;
      const lon = state.lon;
      let key = "87d1c5e946728f6a93e632277bb15dc4";
      const api = `https://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${lon}&cnt=4&appid=${key}`;
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      fetch(api, requestOptions)
        .then((response) => response.json())
        .then((data) => {
          data.list.forEach((obj, idx) => {
            forecastArr.push({
              daytemp: convertToFarenheit(obj.temp.day),
              description: obj.weather[0].description,
              maxTemp: convertToFarenheit(obj.temp.max),
              minTemp: convertToFarenheit(obj.temp.min),
            });
          });
          self.setState({ forecast: forecastArr });
        })

        .catch((error) => console.log("error", error));
    }
    success();
  };

    getNewCoordinate = async () => {
    let self = this;

    function success(position) {
      let forecastArr = [];
      let key = "AIzaSyBMM_9moMb0YT44mtEZnzqjJReeFFHJwqg";
      let address = self.state.newSearch;
      const api = `https://maps.google.com/maps/api/geocode/json?address=${address}&key=${key}`;
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      fetch(api, requestOptions)
        .then((response) => response.json())
        .then((data) => {
          console.log(
            `NEW COORDINATES DATA`,
            data.results[0].geometry.location
          );

          self.setState({ lat: data.results[0].geometry.location.lat });
          self.setState({ lon: data.results[0].geometry.location.lng });
          self.getNewHourlyWeather();
        })

        .catch((error) => console.log("error", error));
    }
    function error() {
      return "Unable to retrieve your location";
    }
    navigator.geolocation.getCurrentPosition(success, error);
  };


  getHourlyWeather = async () => {
    let self = this;
    const { convertToFarenheit, state } = this;
    function success(position) {
      let hourlyArr = [];
      const lat = state.lat;
      const lon = state.lon;
      let key = "87d1c5e946728f6a93e632277bb15dc4";
      const api = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=minutely, alerts&appid=${key}`;
      console.log("blah blah state from getHourly", state)
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      fetch(api, requestOptions)
        .then((response) => response.json())
        .then((data) => {
          // console.log(`ONE CALL API HOURLY INITIAL`, data)
          data.hourly.forEach((obj, idx) => {
            if (idx < 10) {
              hourlyArr.push({
                temp: convertToFarenheit(obj.temp),
                description: obj.weather[0].description,
                dt: obj.dt + data.timezone_offset,
              });
            }
          });
          hourlyArr.push(
            {
              dt: data.current.sunrise + data.timezone_offset,
              sunrise: true,
              temp: "",
              description: "",
            },
            {
              dt: data.current.sunset + data.timezone_offset,
              sunset: true,
              temp: "",
              description: "",
            }
          );

          self.setState({
            sunrise: data.current.sunrise + data.timezone_offset,
          });
          self.setState({ sunset: data.current.sunset + data.timezone_offset });
          let sorted = hourlyArr.sort((a, b) => {
            return a.dt - b.dt;
          });

          self.setState({ hourlyArr: sorted });
          self.setState({ currentDt: data.current.dt + data.timezone_offset });
          if (
            self.state.currentDt > self.state.sunset ||
            self.state.currentDt < self.state.sunrise
          ) {
            self.setState({ backgroundColor: "#000000" });
          }

          self.setState({
            iconAnimating: false,
          });
        })

        .catch((error) => console.log("error", error));
    }
    success();
  };

    getNewHourlyWeather = async () => {
    let self = this;
    const { convertToFarenheit, state } = this;
    self.setState({ currentDt: "" });
    self.setState({ hourlyArr: [] });
   
    function success(position) {
      let hourlyArr = [];
      const lat = state.lat;
      const long = state.lon;
      let key = "87d1c5e946728f6a93e632277bb15dc4";
      const api = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${long}&exclude=minutely, alerts&appid=${key}`;
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      fetch(api, requestOptions)
        .then((response) => response.json())
        .then((data) => {
          // console.log(`ONE CALL API`, data)
          data.hourly.forEach((obj, idx) => {
            if (idx < 10) {
              hourlyArr.push({
                temp: convertToFarenheit(obj.temp),
                description: obj.weather[0].description,
                dt: obj.dt + data.timezone_offset,
              });
            }
          });
          hourlyArr.push(
            {
              dt: data.current.sunrise + data.timezone_offset,
              sunrise: true,
              temp: "",
              description: "",
            },
            {
              dt: data.current.sunset + data.timezone_offset,
              sunset: true,
              temp: "",
              description: "",
            }
          );
          self.setState({ currentDt: data.current.dt + data.timezone_offset });
          self.setState({
            sunrise: data.current.sunrise + data.timezone_offset,
          });
          self.setState({ sunset: data.current.sunset + data.timezone_offset });
          let sorted = hourlyArr.sort((a, b) => {
            return a.dt - b.dt;
          });

          self.setState({ hourlyArr: sorted });
          self.setState({ currentDt: data.current.dt });

          ///CHANGE BLACK BACKGROUND
          if (
            state.currentDt + data.timezone_offset > state.sunset ||
            state.currentDt + data.timezone_offset < state.sunrise
          ) {
            self.setState({ backgroundColor: "#000000" });
          }
          console.log(`NEW HOURLY DATA`, data);
         // console.log(`NEW HOURLY WEATHER TEST`, self.state);
          self.setState({
            iconAnimating: false,
          });
        })

        .catch((error) => console.log("error", error));
    }
    success();
  };


  getNewForecast = async () => {
    let self = this;
    const { convertToFarenheit, state, setState, getHoliday } = this;
    setState({ holiday: "" });
    function success(position) {
      setState({ forecast: [] });
      let forecastArr = [];
      let key = "87d1c5e946728f6a93e632277bb15dc4";
      const api = `https://api.openweathermap.org/data/2.5/forecast/daily?q=${state.newSearch}&cnt=4&appid=${key}`;
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      fetch(api, requestOptions)
        .then((response) => response.json())
        .then((data) => {
          data.list.forEach((obj, idx) => {
            forecastArr.push({
              daytemp: convertToFarenheit(obj.temp.day),
              description: obj.weather[0].description,
              maxTemp: convertToFarenheit(obj.temp.max),
              minTemp: convertToFarenheit(obj.temp.min),
            });
          });

          setState({ forecast: forecastArr });
          getHoliday();
          
        })

        .catch((error) => console.log("error", error));
    }
    success();
  };


  getNewWeather = async () => {
    
    let self = this;
    self.setState({toggle: false});
    const { convertToFarenheit } = this;

    function success(position) {
      let weatherObj = {};
      let key = "87d1c5e946728f6a93e632277bb15dc4";
      //OLD SEARCH USED self.state.newSearch
      const api = `https://api.openweathermap.org/data/2.5/weather?q=${self.state.newSearch}&appid=${key}`;
       const api2 = `https://api.openweathermap.org/data/2.5/weather?q=${self.state.backupDescription}&appid=${key}`;
      let myWeather = fetch(api)
        .then((response) => response.json())
        .then((data) => {
          
          weatherObj.name = data.name + ", ==> name of the city";
          self.setState({ cityName: data.name });
          weatherObj.description =
            data.weather[0].description + ", ==> weather description";
          if (
            data.weather[0].description.includes("cloud") ||
            
            data.weather[0].description.includes("haze") ||
            data.weather[0].description.includes("mist")
          ) {
            self.setState({ backgroundColor: "#ADAA9E" });
          } else if (weatherObj.description.includes("clear")) {
            self.setState({ backgroundColor: "#FFC933" });
          } else if (weatherObj.description.includes("rain") ||
          data.weather[0].description.includes("fog")  ||
           weatherObj.description.includes("smoke")) {
            self.setState({ backgroundColor: "#E0E3E4" });
          } else if (weatherObj.description.includes("snow")) {
            self.setState({ backgroundColor: "#FFFFFF" });
          } else {
            self.setState({ backgroundColor: "D1FFBB" });
          }

          //console.log(`NEW WEATHER COUNTRY DATA`, data.sys.country)
          self.setState({ country: data.sys.country });
          self.setState({ description: data.weather[0].description });
          weatherObj.temperature =
            convertToFarenheit(data.main.temp) + ", ==> temperature";
          weatherObj.temperature_min =
            convertToFarenheit(data.main.temp_min) + ", ==> temperature min";
          self.setState({ low: convertToFarenheit(data.main.temp_min) });
          weatherObj.temperature_max =
            convertToFarenheit(data.main.temp_max) + ", ==> temperature max";
          self.setState({ high: convertToFarenheit(data.main.temp_max) });
          self.state.temperature = convertToFarenheit(data.main.temp);
          self.setState({ temperature: convertToFarenheit(data.main.temp) });
          weatherObj.wind = data.wind.speed + ", ==> wind speed";
          self.setState({ windspeed: data.wind.speed });
          weatherObj.humidity = data.main.humidity + ", ==> humidity";
          console.log(`MY NEW WEATHER`, weatherObj);
          
          self.setState({ newSearch: "" });
          
        })
        .catch((err) => {console.log(`Main Weather search failed, trying new weather`);
        
         let myWeather = fetch(api2)
        .then((response) => response.json())
        .then((data) => {
          
          weatherObj.name = data.name + ", ==> name of the city";
          self.setState({ cityName: data.name });
          weatherObj.description =
            data.weather[0].description + ", ==> weather description";
          if (
            data.weather[0].description.includes("cloud") ||
            
            data.weather[0].description.includes("haze") ||
            data.weather[0].description.includes("mist")
          ) {
            self.setState({ backgroundColor: "#ADAA9E" });
          } else if (weatherObj.description.includes("clear")) {
            self.setState({ backgroundColor: "#FFC933" });
          } else if (weatherObj.description.includes("rain") ||
          data.weather[0].description.includes("fog")  ||
           weatherObj.description.includes("smoke")) {
            self.setState({ backgroundColor: "#E0E3E4" });
          } else if (weatherObj.description.includes("snow")) {
            self.setState({ backgroundColor: "#FFFFFF" });
          } else {
            self.setState({ backgroundColor: "D1FFBB" });
          }

          //console.log(`NEW WEATHER COUNTRY DATA`, data.sys.country)
          self.setState({ country: data.sys.country });
          self.setState({ description: data.weather[0].description });
          weatherObj.temperature =
            convertToFarenheit(data.main.temp) + ", ==> temperature";
          weatherObj.temperature_min =
            convertToFarenheit(data.main.temp_min) + ", ==> temperature min";
          self.setState({ low: convertToFarenheit(data.main.temp_min) });
          weatherObj.temperature_max =
            convertToFarenheit(data.main.temp_max) + ", ==> temperature max";
          self.setState({ high: convertToFarenheit(data.main.temp_max) });
          self.state.temperature = convertToFarenheit(data.main.temp);
          self.setState({ temperature: convertToFarenheit(data.main.temp) });
          weatherObj.wind = data.wind.speed + ", ==> wind speed";
          self.setState({ windspeed: data.wind.speed });
          weatherObj.humidity = data.main.humidity + ", ==> humidity";
          console.log(`MY NEW WEATHER`, weatherObj);
          
          self.setState({ newSearch: "" });
          
        }).catch((error) => console.log("BACK UP ERROR", error));
        
        }
        
        );
    }
    function error() {
      return "Unable to retrieve your location";
    }
    navigator.geolocation.getCurrentPosition(success, error);
  };



  render() {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    let loadingColor="E0F9C7"
    let clearColor = "#FFC933";
    let nightColor = "#000000";
    let cloudColor = "#ADAA9E";
    let rainColor = "E0E3E4";
    let snowColor = "#FFFFFF";
    let textColor =this.state.backgroundColor === nightColor? StylingNight.text : null;
    let modalColorNight =
      this.state.backgroundColor === nightColor ||
      this.state.backgroundColor === cloudColor ||
      this.state.backgroundColor === snowColor ||
      this.state.description.includes("rain")
        ? StylingNight.modalView
        : Styling.modalView;

    let bg = this.state.backgroundColor;

  if (this.state.showSplash) { 
    return (
      <LoadingScene />
    )

  }
  else {
    return (
      <View
        style={{
          flex: 1,
          padding: 1,
          paddingTop: Constants.statusBarHeight + 10,
          backgroundColor: this.state.iconAnimating? loadingColor: bg,
          color: "#9C27B0",
        }}
      > 
     
         <View  style={Styling.topDiv}> 
        <TouchableOpacity
          onPress={() => this.setState({ showSearch: !this.state.showSearch })}
        >
          <Image
            style={Styling.tinyLogo}
            source={require("./assets/world.png")}
          />
        </TouchableOpacity>
        <View style={Styling.topRight}>
        <Text style={textColor}>F °</Text>
        <Switch
          trackColor={{false: 'gray', true: '#D2E6B9'}}
          thumbColor="white"
          ios_backgroundColor="gray"
          onValueChange={() => {this.toggleSwitch()}}
          value={this.state.toggle}
        />
        <Text style={textColor}>C °</Text>
        </View>

         <Icon
    name="question"
    color={bg==nightColor? "white": "black"}
    style={{marginTop: 15,}}
    size={40}
    onPress={() => this.setState({showAbout: !this.state.showAbout})}
  >
  </Icon>
 
  </View> 


        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showSearch}
          onRequestClose={() => {
            console.log("Modal has been closed.");
          }}
        >
          {this.state.showSearch ? (
            <View style={modalColorNight}>
              <GooglePlacesAutocomplete
                styles={{
                  container: {
                    //  height: 50,
                  },
                  textInputContainer: {
                    //backgroundColor: 'grey',
                    width: "100%",
                    margin: "auto",
                    // height: 40,
                  },
                  textInput: {
                    height: 38,
                    color: "#5d5d5d",
                    fontSize: 16,
                  },
                  predefinedPlacesDescription: {
                    color: "#1faadb",
                  },
                }}
                placeholder="Search"
                query={{
                  key: GOOGLE_PLACES_API_KEY,
                  language: "en", // language of the results
                }}
                onPress={(data, details) => {
                  this.setState({ newSearch: data.description });
                  //console.log(`Checking backUp CityName`, data.structured_formatting.main_text);
                  this.getNewWeather();
                  this.getNewForecast();
                  this.getNewCoordinate();
                  this.setState({
                    iconAnimating: true,
                  });
                  this.setState({ showSearch: !this.state.showSearch });
                  this.setState({ backupDescription: data.structured_formatting.main_text });
                
                }}
               
                onFail={(error) => console.error(error)}
                requestUrl={{
                  url:
                    "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api",
                  useOnPlatform: "web",
                }} // this in only required for use on the web. See https://git.io/JflFv more for details.
              />
              <TouchableOpacity
                onPress={() =>
                  this.setState({ showSearch: !this.state.showSearch })
                }
              >
                <Icon
                  name="down"
                  color={bg == nightColor ? "white" : "black"}
                  size={55}
                  //onPress={this.loginWithFacebook}
                ></Icon>
              </TouchableOpacity>
            </View>
          ) : null}
        </Modal>

          <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showAbout}
          onRequestClose={() => {
            console.log("Modal has been closed.");
          }}
        >
        
            <View style={modalColorNight}>
            <View style={{marginTop: 70,
            alignItems: "center",}}>
                <Text style={textColor}>Created by Carlos Planchart.  </Text>
                <Text style={{color: 'blue',
                textDecorationLine: 'underline',
                marginTop: 10,
                marginBottom: 10,
                }}
      onPress={() => Linking.openURL('https://cplan485.gitlab.io/portfolio-project/')}>
              See other projects here
            </Text>
            <Text style={textColor}>All icons created by me. © 2021</Text>
              
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ showAbout: !this.state.showAbout })
                }
              >
                <Icon
                  name="down"
                  style={{marginTop: 150, }}
                  color={bg == nightColor ? "white" : "black"}
                  size={55}
                ></Icon>
              </TouchableOpacity>
            </View>
          </Modal>
        

        <StatusBar
          style="auto"
          barStyle={bg == nightColor ? "light-content" : "default"}
        />
        <ScrollView style={Styling.scrollView}>
          <LoadingIcon isIconAnimating={this.state.iconAnimating} />
          {this.state.iconAnimating === false ? (
            <View>
            <MainWeather
              temperature={this.state.temperature}
              backgroundColor={this.state.backgroundColor}
              description={this.state.description}
              backupDescription={this.state.backupDescription}
              high={this.state.high}
              low={this.state.low}
              cityName={this.state.cityName}
              getWeekDay={this.getWeekDay}
              holiday={this.state.holiday}
            />
            <Forecast
            forecast={this.state.forecast}
            getWeekDay={this.getWeekDay}
            backgroundColor={this.state.backgroundColor}
          />
          <HourlyForecast
            hourlyArr={this.state.hourlyArr}
            sunset={this.state.sunset}
            sunrise={this.state.sunrise}
            backgroundColor={this.state.backgroundColor}
          /></View>
          ) : null}

        
        </ScrollView>
      </View>
    );
  }
  }
}

// export default App;