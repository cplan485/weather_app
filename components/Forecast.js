import React from 'react';
import { View, Text, Image } from 'react-native';
import Styling from './Styling';
import StylingNight from './StylingNight';


class Forecast extends React.Component {
 
  render () {
    
    var today = new Date();
    let time = today.getHours();
    
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    
    const daysOfWeek= ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];

    const index = daysOfWeek.indexOf(this.props.getWeekDay(today));

    let snowColor="#FFFFFF";
    let nightColor="#000000";
    let midColor = this.props.backgroundColor === nightColor? StylingNight.midText : Styling.midText;
    let titleColor = this.props.backgroundColor === nightColor? StylingNight.titleText : Styling.titleText;
    let largeColor =this.props.backgroundColor === nightColor? StylingNight.largeText : Styling.largeText;
    let textColor =this.props.backgroundColor === nightColor? StylingNight.text : null;

    let renderForecast =(arr) => (
      arr.map( (item, idx) => {
       // console.log(`original array`, item.text)
        return <View 
        style={Styling.mainWeather}
        key={idx}>
          <Text style={midColor}> {index+1+idx > 6? daysOfWeek[index+idx -6] : daysOfWeek[index+1 +idx]} </Text>
          <Text style={titleColor}>{item.daytemp} °</Text>
          {item.description.includes("cloud") ? <Image
        style={Styling.smallLogo}
        source={require('../assets/cloud.png')} />  :
        item.description.includes("haze") ? <Image
        style={Styling.smallLogo}
        source={require('../assets/mist.png')} />  :  
        item.description.includes("broken clouds") || item.description.includes("few clouds") || item.description.includes("scattered clouds")   ? <Image
        style={Styling.smallLogo}
        source={require('../assets/brokenCloud.png')} />  : 
        item.description.includes("rain") ? <Image
        style={Styling.smallLogo}
        source={require('../assets/rain.png')} /> :
        item.description.includes("snow") ? <Image
        style={Styling.smallLogo}
        source={require('../assets/snow3.png')} /> :
        <Image
        style={Styling.smallLogo}
        source={require('../assets/sun.png')} 
      />}
          {/* <Text>{item.description}</Text>  */}
          <Text style={textColor}>H: {item.maxTemp} °</Text>
          <Text style={textColor}>L: {item.minTemp} °</Text>
      </View>
      })
    )
    return(
      <View style={Styling.forecastDiv}>
       {renderForecast(this.props.forecast)}
       </View>
    )
  }
}

export default Forecast;