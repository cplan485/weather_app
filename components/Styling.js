import React from 'react';
import { StyleSheet } from 'react-native';


const Styling = StyleSheet.create({
 
    modalView: {
     margin: 20,
     backgroundColor: "rgba(255, 165, 0, 0.73)",
     borderRadius: 20,
     height: 400,
     padding: 35,
     alignItems: "center",
     shadowColor: "#000",
     shadowOffset: {
       width: 0,
       height: 2
     },
    },

   image: {
      flex: 1,

   },
   test: {
    color: "#A1FF33",

 },
 holidayText: {
  marginLeft: 15,
  marginRight: 15,
},
   midText: {
     fontSize: 15,
     fontWeight: "bold"
   },
   titleText: {
     fontSize: 20,
     fontWeight: "bold",
     marginBottom: 10,
   },
    splashText: {
     fontSize: 24,
     fontWeight: "bold",
     marginTop: 10,
   },
   largeText: {
     fontSize: 70,
     fontWeight: "bold",
     marginTop: 5,
     marginBottom: 5,
   },
    mainWeather: {
     flex: 1,
     flexDirection: 'column',
     justifyContent: 'center',
     alignItems: 'center',
    //  textAlign: "center",
     marginBottom: 20,
     marginTop: 20,
   },
 
   tinyLogo: {
     margin: 10,
     padding: 10,
     width: 75,
     height: 75,
   },
   smallLogo: {
      margin: "auto",
      padding: 15,
     width: 50,
     height: 50,
   },
   smallHourlyLogo: {
    width: 50,
    height: 50,
  },
   mainLogo: {
     margin: 20,
     width: 250,
     height: 250,
     alignItems: "center",
   
   },
   forecastDiv: {
     // backgroundColor: "rgba(255, 165, 0, 0.73)",
     marginTop: 10,
     display: "flex",
     flexDirection: 'row',
     justifyContent: 'space-between',
     flex: 1,
     margin: "auto",
 
   },
 
   topDiv: {
    display: "flex",
    flexDirection: 'row',
    justifyContent: 'space-between',
   },
    topRight: {
    display: "flex",
    flexDirection: 'row',
    marginTop: 22,
    marginLeft: 50,
   },
    hourlyContainer: {
     // backgroundColor: "rgba(255, 165, 0, 0.73)",
     alignItems: "center",
     justifyContent: 'center',
 
   },
 
   hourly: {
     marginTop: 20,
     marginBottom: 20,
     display: "flex",
     flexDirection: 'row',
     justifyContent: 'space-between',
     alignItems: "center",
     marginLeft: "15%",
      width: "70%",
      
   },
 });


export default Styling;