import React from 'react';
import { View, StyleSheet, TextInput, Text, Button, Image, ImageBackground,  } from 'react-native';
import Styling from './Styling';
import StylingNight from './StylingNight';


class MainWeather extends React.Component {
 
  render () {

    let Moon = {
      phases: ['new-moon', 'waxing-crescent-moon', 'quarter-moon', 'waxing-gibbous-moon', 'full-moon', 'waning-gibbous-moon', 'last-quarter-moon', 'waning-crescent-moon'],
      phase: function (year, month, day) {
        let c,e,jd,b = 0;
    
        if (month < 3) {
          year--;
          month += 12;
        }
    
        ++month;
        c = 365.25 * year;
        e = 30.6 * month;
        jd = c + e + day - 694039.09; // jd is total days elapsed
        jd /= 29.5305882; // divide by the moon cycle
        b = parseInt(jd); // int(jd) -> b, take integer part of jd
        jd -= b; // subtract integer part to leave fractional part of original jd
        b = Math.round(jd * 8); // scale fraction from 0-8 and round
    
        if (b >= 8) b = 0; // 0 and 8 are the same so turn 8 into 0
        return {phase: b, name: Moon.phases[b]};
      }
    };

    
    
    var today = new Date();
    let time = today.getHours();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    var MoonDd = today.getDate()
    var MoonMm = today.getMonth() + 1;

    today = mm + '/' + dd + '/' + yyyy;

    let clearColor="#FFC933"
    let cloudColor="#ADAA9E";
    let rainColor="E0E3E4";
    let snowColor="#FFFFFF";
    let nightColor="#000000";
    let titleColor = this.props.backgroundColor === nightColor? StylingNight.titleText : Styling.titleText;
    let largeColor =this.props.backgroundColor === nightColor? StylingNight.largeText : Styling.largeText;
    let textColor =this.props.backgroundColor === nightColor? StylingNight.text : null;
    let holidayTextColor =this.props.backgroundColor === nightColor? StylingNight.holidaytext : Styling.holidayText;

    let moonCycle = this.props.backgroundColor === nightColor? <Text style={textColor}> {Moon.phase(yyyy, MoonMm, MoonDd).name}</Text> : null;

    return(
      <View><ImageBackground source={this.props.description.includes("clear")? require('../assets/Mountains1.svg') : null} 
       
       style={Styling.image}>
         <View style={Styling.mainWeather}>
            <View style={Styling.mainWeather}>   
       <Text style={titleColor}> {this.props.getWeekDay(today)} - {today} </Text>
       {this.props.holiday !=''? <Text style={holidayTextColor}>{this.props.holiday}</Text> : null }
       {this.props.description.includes("snow") && this.props.backgroundColor ===nightColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/snowyNight.png')} /> :
        (this.props.description.includes("haze") || this.props.description.includes("mist") ) && this.props.backgroundColor ===nightColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/hazyNight.png')} /> :
        this.props.description.includes("cloud") && this.props.backgroundColor ===nightColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/cloudyNight.png')} /> :
        this.props.description.includes("rain") && this.props.backgroundColor ===nightColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/rainyNight.png')} /> :
        this.props.description.includes("fog") && this.props.backgroundColor ===nightColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/foggyNight.png')} /> :
        this.props.backgroundColor === nightColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/Moon.png')} /> :
        this.props.description.includes("broken clouds") || this.props.description.includes("few clouds") ? <Image
        style={Styling.mainLogo}
        source={require('../assets/brokenCloud.png')} /> :
        this.props.description.includes("mist") || this.props.description.includes("haze") ? <Image
        style={Styling.mainLogo}
        source={require('../assets/mist.png')} /> :
        this.props.description.includes("smoke") || this.props.description.includes("fog") ? <Image
        style={Styling.mainLogo}
        source={require('../assets/smoke.png')} /> :
       this.props.backgroundColor ===cloudColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/cloud.png')} />  :
         this.props.description.includes("rain") ? <Image
        style={Styling.mainLogo}
        source={require('../assets/rain.png')} /> : 
        this.props.backgroundColor ===snowColor ? <Image
        style={Styling.mainLogo}
        source={require('../assets/snow3.png')} /> :
        this.props.backgroundColor ===clearColor ?
        <Image
        style={Styling.mainLogo}
        source={require('../assets/sun.png')} 
      /> : null}
       <Text style={textColor}>It is</Text>
       <Text style={largeColor}> {this.props.temperature} °</Text>
       <Text style={textColor}>{this.props.description}</Text>
       <Text style={titleColor}> H: {this.props.high} ° | L: {this.props.low} °</Text>
       
       <Text style={titleColor}>in {this.props.cityName !==undefined? this.props.cityName : this.props.backupDescription}</Text>
       {moonCycle}
        </View>
       </View>
       </ImageBackground></View>
    )
  }
}

export default MainWeather;