import React,{Component} from 'react';
import {View, ImageBackground, Image, Text} from 'react-native';
import Styling from './Styling';

var bg=require('../assets/Mountains1.svg');
var logo=require('../assets/mainLogo.png')
class LoadingScene extends Component { 

    

   render ()
   {
       return(
           <ImageBackground
           source={bg}
            style={{height:'100%',
        width: '100%',
    backgroundColor: "#E0E3E4"}}
           >
               <View
               style={{flex:1, 
               justifyContent: 'center',
               alignItems: 'center',}}>
                   <Image source={logo}
                   style={{height: 250, width: 250}}
                   >
                   </Image>
                <Text style={Styling.splashText}>Nebulosity</Text>
               </View>

           </ImageBackground>
       );
   } 
}

export default LoadingScene;