import React from 'react';
import { View, Text, Image } from 'react-native';
import Styling from './Styling';
import StylingNight from './StylingNight';
import Icon from 'react-native-vector-icons/Feather';


class HourlyForecast extends React.Component {

  render () {

    let nightColor="#000000";
    let midColor = this.props.backgroundColor === nightColor? StylingNight.midText : Styling.midText;
    let titleColor = this.props.backgroundColor === nightColor? StylingNight.titleText : Styling.titleText;
    let largeColor =this.props.backgroundColor === nightColor? StylingNight.largeText : Styling.largeText;
    let textColor =this.props.backgroundColor === nightColor? StylingNight.text : null;

    function convertTimestamptoTime(unixTimestamp) { 
    
      let dateObj = new Date(unixTimestamp * 1000); 
      let utcString = dateObj.toUTCString(); 

      let time = utcString.slice(-12, -7); 

     return time;
  } 
    
    let renderHourly =(arr) => (
      arr.map( (item, idx) => {
         let nightTime = item.dt > this.props.sunset || item.dt < this.props.sunrise
       // console.log(`original array`, item.text)
       if(
          // item.dt >= this.state.currentDt && 
         idx < 10
         ) {
         if( item.sunset) {
           return <View key={idx} style={Styling.hourly}>
             <Text style={midColor}>
             {convertTimestamptoTime(item.dt)}
             </Text>
             <Text style={midColor}>
             Sunset  
           </Text>
         
          <Image
          style={Styling.smallHourlyLogo}
            source={require('../assets/sunset.png')} />
           </View>
         }
         else if (item.sunrise) {
          return <View key={idx} style={Styling.hourly}>
          <Text style={midColor}>
          {convertTimestamptoTime(item.dt)}
          </Text>
          <Text style={midColor}>
          Sunrise  
        </Text>
        
         <Image
       style={Styling.smallHourlyLogo}
         source={require('../assets/sunrise.png')} /> 
        </View>
         }
        else 
        return <View 
        style={Styling.hourly}
        key={idx}>
          <Text style={midColor}>
             {/* {time + 1 + idx}:00  */}
             {convertTimestamptoTime(item.dt)}
             </Text>
          <Text style={midColor}>{item.temp} °</Text>
          <View>
          { nightTime && item.description.includes("haze") ? 
        <Image
          style={Styling.smallHourlyLogo}
            source={require('../assets/hazyNight.png')} /> 
            :
            nightTime && item.description.includes("cloud") ? 
        <Image
          style={Styling.smallHourlyLogo}
            source={require('../assets/cloudyNight.png')} /> 
            :
            nightTime && item.description.includes("snow") ? 
            <Image
              style={Styling.smallHourlyLogo}
                source={require('../assets/snowyNight.png')} /> 
                :
                nightTime && item.description.includes("rain") ? 
                <Image
                  style={Styling.smallHourlyLogo}
                    source={require('../assets/rainyNight.png')} /> 
                    :
          nightTime ? 
        <Image
          style={Styling.smallHourlyLogo}
            source={require('../assets/Moon.png')} /> 
            : 
            item.description.includes("haze") ? <Image
            style={Styling.smallLogo}
            source={require('../assets/mist.png')} />  :
            item.description.includes("smoke") || item.description.includes("fog") ? <Image
            style={Styling.smallLogo}
            source={require('../assets/smoke.png')} />  :  
            item.description.includes("broken clouds") || item.description.includes("few clouds") || item.description.includes("scattered clouds")   ? <Image
            style={Styling.smallLogo}
            source={require('../assets/brokenCloud.png')} />  :
            item.description.includes("cloud") ? <Image
        style={Styling.smallHourlyLogo}
        source={require('../assets/cloud.png')} />  : item.description.includes("rain") ? <Image
        style={Styling.smallHourlyLogo}
        source={require('../assets/rain.png')} /> :
        item.description.includes("mist") || item.description.includes("haze") ? <Image
        style={Styling.smallHourlyLogo}
        source={require('../assets/mist.png')} /> :
        item.description.includes("snow") ? <Image
        style={Styling.smallHourlyLogo}
        source={require('../assets/snow3.png')} /> :
        <Image
        style={Styling.smallHourlyLogo}
        source={require('../assets/sun.png')} 
      />

          }

            {/* <Text> {item.description}</Text>  */}
          </View>
      </View> }
      })
    )

    return(
      <View style={Styling.hourlyContainer}>
      <View style={Styling.hourlyDiv}>
   {renderHourly(this.props.hourlyArr)}
      </View>
   </View>
    )
  }
}

export default HourlyForecast;