# Nebulosity Weather App
React Native Weather App that shows current weather, four day forecast, and hourly weather with Icons developed in Processing.

# How to Run It

First clone the repo, then:
### `expo start`

Make sure that expo is installed or: 
### `npm i expo`